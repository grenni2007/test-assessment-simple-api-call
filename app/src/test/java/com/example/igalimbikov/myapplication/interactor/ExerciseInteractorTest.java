package com.example.igalimbikov.myapplication.interactor;

import com.example.igalimbikov.myapplication.di.AppComponent;
import com.example.igalimbikov.myapplication.di.DaggerAppComponent;
import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.Partner;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;
import com.example.igalimbikov.myapplication.repository.ExerciseRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ExerciseInteractorTest {
    private final PartnerAvailability partner1 = new PartnerAvailability(
            new Partner(
                    "ddaignault@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 4, 3),
                    new Date(2017, 4, 6)));


    private final PartnerAvailability partner2 = new PartnerAvailability(
            new Partner(
                    "cbrenna@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 27),
                    new Date(2017, 3, 29),
                    new Date(2017, 3, 30)));

    private final PartnerAvailability partner3 = new PartnerAvailability(
            new Partner("jgustison@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 29),
                    new Date(2017, 3, 30),
                    new Date(2017, 4, 1)));


    private final PartnerAvailability partner4 = new PartnerAvailability(
            new Partner("tmozie@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 28),
                    new Date(2017, 3, 29),
                    new Date(2017, 4, 1),
                    new Date(2017, 4, 4)));

    private final PartnerAvailability partner5 = new PartnerAvailability(
            new Partner("taffelt@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 28),
                    new Date(2017, 3, 29),
                    new Date(2017, 4, 2),
                    new Date(2017, 4, 4)));

    private final PartnerAvailability partner6 = new PartnerAvailability(
            new Partner("ryarwood@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 29),
                    new Date(2017, 3, 30),
                    new Date(2017, 4, 2),
                    new Date(2017, 4, 3)));

    private final PartnerAvailability partner7 = new PartnerAvailability(
            new Partner("sfilipponi@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 30),
                    new Date(2017, 4, 1)));

    private final PartnerAvailability partner8 = new PartnerAvailability(
            new Partner("omajica@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 28),
                    new Date(2017, 3, 29),
                    new Date(2017, 4, 1),
                    new Date(2017, 4, 3)));

    private final PartnerAvailability partner9 = new PartnerAvailability(
            new Partner("wzartman@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 3, 29),
                    new Date(2017, 3, 30),
                    new Date(2017, 4, 2),
                    new Date(2017, 4, 3)));

    private final PartnerAvailability partner10 = new PartnerAvailability(
            new Partner("eauther@hubspotpartners.com"),
            Arrays.asList(
                    new Date(2017, 4, 4),
                    new Date(2017, 4, 9)));

    private final CountryEvent irelandEvent = new CountryEvent(
            new Country("Ireland"),
            Collections.singletonList(new Partner("cbrenna@hubspotpartners.com"))
            , new Date(2017, 3, 29));

    private final CountryEvent usEvent = new CountryEvent(
            new Country("United States"),
            new ArrayList<Partner>(),
            null);

    private final CountryEvent spainEvent = new CountryEvent(
            new Country("Spain"),
            Arrays.asList(
                    new Partner("omajica@hubspotpartners.com"),
                    new Partner("taffelt@hubspotpartners.com"),
                    new Partner("tmozie@hubspotpartners.com"))
            , new Date(2017, 3, 28));


    @Test
    //TODO in real project should be in placed in integration tests group,
    // because can be unstable due to network and API
    //I used this method to submit my solution
    public void shouldCalculateCorrectWithRealApiCall() {
        AppComponent appComponent = DaggerAppComponent.builder()
                .build();
        ExerciseInteractor exerciseInteractor = appComponent.getExerciseInteractor();
        assertPerformTestExerciseWasCompleted(exerciseInteractor);
    }

    @Test
    public void shouldCalculateCorrectWithMockedExample() {
        ExerciseRepository repository = Mockito.mock(ExerciseRepository.class);
        Map<Country, List<PartnerAvailability>> map = new HashMap<>();
        map.put(new Country("United States"), Arrays.asList(partner1, partner10));
        map.put(new Country("Ireland"), Collections.singletonList(partner2));
        map.put(new Country("Spain"), Arrays.asList(partner3, partner4, partner5, partner6, partner7, partner8, partner9));

        Mockito.when(repository.getInput()).thenReturn(Single.just(map));
        List<CountryEvent> events = Arrays.asList(irelandEvent, spainEvent, usEvent);
        Mockito.when(repository.submitResult(events)).thenReturn(Completable.complete());
        ExerciseInteractor interactor = new ExerciseInteractor(repository);
        assertPerformTestExerciseWasCompleted(interactor);
    }

    private void assertPerformTestExerciseWasCompleted(ExerciseInteractor interactor) {
        TestObserver testObserver = new TestObserver<>();
        interactor.performTestExercise().subscribe(testObserver);
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
        testObserver.assertComplete();
    }
}
