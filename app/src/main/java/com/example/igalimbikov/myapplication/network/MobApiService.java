package com.example.igalimbikov.myapplication.network;

import com.example.igalimbikov.myapplication.data.remote.model.ApiHubSpotPartners;
import com.example.igalimbikov.myapplication.data.remote.request.ResultRequestBody;

import io.reactivex.Completable;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public interface MobApiService {

    @GET("partners")
    Observable<ApiHubSpotPartners> getPartners(@Query("userKey") String key);

    @POST("results")
    Completable submitCountryEventsSchedule(@Query("userKey") String key, @Body ResultRequestBody body);
}
