package com.example.igalimbikov.myapplication.di;

import com.example.igalimbikov.myapplication.network.MobApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i.galimbikov on 19/11/2017.
 */
@Module
public class NetworkModule {
    private static final String HUB_SPOT_BASE_URL = "https://candidate.hubteam.com/candidateTest/v2/";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(HUB_SPOT_BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    MobApiService provideHubSpotService(Retrofit retrofit) {
        return retrofit.create(MobApiService.class);
    }
}
