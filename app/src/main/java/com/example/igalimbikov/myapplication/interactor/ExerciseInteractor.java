package com.example.igalimbikov.myapplication.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.Partner;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;
import com.example.igalimbikov.myapplication.repository.ExerciseRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Singleton
public class ExerciseInteractor {
    private final ExerciseRepository repository;

    @Inject
    public ExerciseInteractor(ExerciseRepository repository) {
        this.repository = repository;
    }

    public Completable performTestExercise() {
        Single<Map<Country, List<PartnerAvailability>>> input = repository.getInput();
        return input.toObservable().flatMapIterable(new Function<Map<Country, List<PartnerAvailability>>, Iterable<Map.Entry<Country, List<PartnerAvailability>>>>() {
            @Override
            public Iterable<Map.Entry<Country, List<PartnerAvailability>>> apply(Map<Country, List<PartnerAvailability>> countryListMap) throws Exception {
                TreeMap<Country, List<PartnerAvailability>> treeMap = new TreeMap<>(new Comparator<Country>() {
                    @Override
                    public int compare(Country countryLeft, Country countryRight) {
                        return countryLeft.getName().compareToIgnoreCase(countryRight.getName());
                    }
                });
                treeMap.putAll(countryListMap);
                return treeMap.entrySet();
            }
        }).map(new Function<Map.Entry<Country, List<PartnerAvailability>>, CountryEvent>() {
            @Override
            public CountryEvent apply(Map.Entry<Country, List<PartnerAvailability>> entry) throws Exception {
                Country country = entry.getKey();
                List<PartnerAvailability> partners = entry.getValue();
                return scheduleCountryEvent(country, partners);
            }
        }).toList().flatMapCompletable(new Function<List<CountryEvent>, CompletableSource>() {
            @Override
            public CompletableSource apply(List<CountryEvent> countryEvents) throws Exception {
                return repository.submitResult(countryEvents);
            }
        });
//        return input.flatMapCompletable(new Function<Map<Country, List<PartnerAvailability>>, CompletableSource>() {
//            @Override
//            public CompletableSource apply(Map<Country, List<PartnerAvailability>> partnersAvailability) throws Exception {
//                List<CountryEvent> eventsSchedule = createEventsSchedule(partnersAvailability);
//                return repository.submitResult(eventsSchedule);
//            }
//        });
    }

    private List<CountryEvent> createEventsSchedule(Map<Country, List<PartnerAvailability>> partnersAvailability) {
        ArrayList<CountryEvent> results = new ArrayList<>();
        for (Map.Entry<Country, List<PartnerAvailability>> partnersAvailabilityPerCountry : partnersAvailability.entrySet()) {
            Country country = partnersAvailabilityPerCountry.getKey();
            List<PartnerAvailability> partners = partnersAvailabilityPerCountry.getValue();
            results.add(scheduleCountryEvent(country, partners));
        }
        return results;
    }

    @NonNull
    private CountryEvent scheduleCountryEvent(Country country, List<PartnerAvailability> countryPartners) {
        SortedMap<Date, Set<Partner>> partnersAvailability = fillPartnerAvailability(countryPartners);
        @Nullable Date date = findBestStartDate(partnersAvailability);
        List<Partner> attendees = getAttendees(partnersAvailability, date);
        return new CountryEvent(country, attendees, date);
    }

    private List<Partner> getAttendees(Map<Date, Set<Partner>> availableDates, @Nullable Date eventStartDate) {
        if (eventStartDate == null) {
            return Collections.emptyList();
        }
        Set<Partner> firstDayAttendees = availableDates.get(eventStartDate);
        Date nextDay = getNextDay(eventStartDate);
        Set<Partner> secondDayAttendees = availableDates.get(nextDay);
        return new ArrayList<>(intersection(firstDayAttendees, secondDayAttendees));
    }

    private Set<Partner> intersection(Set<Partner> firstDayAttendees, Set<Partner> secondDayAttendees) {
        if (firstDayAttendees == null || secondDayAttendees == null) {
            return new HashSet<>();
        }
        Set<Partner> intersection = new HashSet<>(firstDayAttendees);
        intersection.retainAll(secondDayAttendees);
        return intersection;
    }

    private Date getNextDay(Date from) {
        Calendar c = Calendar.getInstance();
        c.setTime(from);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

    /**
     * @param partners list of partners in one country
     * @return Map where:
     * key - date
     * value - list of available partners
     */
    private SortedMap<Date, Set<Partner>> fillPartnerAvailability(List<PartnerAvailability> partners) {
        SortedMap<Date, Set<Partner>> availableDates = new TreeMap<>();
        for (PartnerAvailability partnerAvailability : partners) {
            for (Date date : partnerAvailability.getAvailableDates()) {
                Set<Partner> partnerPerDay = availableDates.get(date);
                if (partnerPerDay == null) {
                    partnerPerDay = new HashSet<>();
                    availableDates.put(date, partnerPerDay);
                }
                partnerPerDay.add(partnerAvailability.getPartner());
            }
        }
        return availableDates;
    }

    @Nullable
    private Date findBestStartDate(SortedMap<Date, Set<Partner>> availableDates) {
        Date bestDate = null;
        int bestDateAttendeesCount = 0;
        for (Date date : availableDates.keySet()) {
            List<Partner> attendees = getAttendees(availableDates, date);
            if (attendees.size() > bestDateAttendeesCount) {
                bestDateAttendeesCount = attendees.size();
                bestDate = date;
            }
        }
        return bestDate;
    }
}
