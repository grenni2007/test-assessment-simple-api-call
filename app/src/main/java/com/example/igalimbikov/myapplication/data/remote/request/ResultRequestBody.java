package com.example.igalimbikov.myapplication.data.remote.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public class ResultRequestBody {

    @SerializedName("countries")
    private final List<RequestCountryEvent> countries;

    public ResultRequestBody(List<RequestCountryEvent> countries) {
        this.countries = countries;
    }
}
