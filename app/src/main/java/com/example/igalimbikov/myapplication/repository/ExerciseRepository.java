package com.example.igalimbikov.myapplication.repository;

import com.example.igalimbikov.myapplication.data.remote.ExerciseDataSource;
import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Singleton
public class ExerciseRepository {
    private final ExerciseDataSource remoteDataSource;

    @Inject
    public ExerciseRepository(ExerciseDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    public Single<Map<Country, List<PartnerAvailability>>> getInput() {
        return remoteDataSource.getInput();
    }

    public Completable submitResult(List<CountryEvent> exerciseResult) {
        return remoteDataSource.submitResult(exerciseResult);
    }
}
