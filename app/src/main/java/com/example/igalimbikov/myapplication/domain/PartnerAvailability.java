package com.example.igalimbikov.myapplication.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by i.galimbikov on 04/12/2017.
 */

public class PartnerAvailability {
    private final Partner partner;

    private final List<Date> availableDates;

    public PartnerAvailability(Partner partner, List<Date> availableDates) {
        this.partner = partner;
        this.availableDates = availableDates;
    }

    public Partner getPartner() {
        return partner;
    }

    public List<Date> getAvailableDates() {
        return availableDates;
    }
}
