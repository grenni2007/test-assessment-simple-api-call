package com.example.igalimbikov.myapplication.data.remote.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@SuppressWarnings("unused")
public class ApiHubSpotPartner {
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("country")
    private String country;
    @SerializedName("availableDates")
    private String[] dates;

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public String[] getDates() {
        return dates;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
