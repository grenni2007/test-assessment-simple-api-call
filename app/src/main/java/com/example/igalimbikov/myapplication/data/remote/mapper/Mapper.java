package com.example.igalimbikov.myapplication.data.remote.mapper;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public interface Mapper<F, T> {
    T transform(F from);
}
