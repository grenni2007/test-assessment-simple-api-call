package com.example.igalimbikov.myapplication.di;

import com.example.igalimbikov.myapplication.data.remote.mapper.ApiHubSpotInputMapper;
import com.example.igalimbikov.myapplication.data.remote.mapper.Mapper;
import com.example.igalimbikov.myapplication.data.remote.mapper.ResultRequestBodyMapper;
import com.example.igalimbikov.myapplication.data.remote.model.ApiHubSpotPartners;
import com.example.igalimbikov.myapplication.data.remote.request.ResultRequestBody;
import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;

import java.util.List;
import java.util.Map;

import dagger.Binds;
import dagger.Module;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Module
public interface ExerciseInteractorModule {
    @Binds
    Mapper<List<CountryEvent>, ResultRequestBody> provideResultRequestBodyMapper(ResultRequestBodyMapper mapper);

    @Binds
    Mapper<ApiHubSpotPartners, Map<Country, List<PartnerAvailability>>> provideApiHubSpotInputMapper(ApiHubSpotInputMapper mapper);
}
