package com.example.igalimbikov.myapplication.di;

import com.example.igalimbikov.myapplication.interactor.ExerciseInteractor;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by i.galimbikov on 19/11/2017.
 */
@Singleton
@Component(modules = {NetworkModule.class, ExerciseInteractorModule.class})
public interface AppComponent {
    ExerciseInteractor getExerciseInteractor();
    //TODO in real android app this interactor should be injected in presenter constructor
}
