package com.example.igalimbikov.myapplication.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public class CountryEvent {
    private final List<Partner> attendees;
    private final Country country;
    private final Date startDate;

    public CountryEvent(Country country, List<Partner> attendees, Date startDate) {
        this.country = country;
        this.attendees = attendees;
        this.startDate = startDate;
    }

    public Country getCountry() {
        return country;
    }

    public List<Partner> getAttendees() {
        return attendees;
    }

    public Date getStartDate() {
        return startDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountryEvent that = (CountryEvent) o;

        if (attendees != null ? !attendees.equals(that.attendees) : that.attendees != null)
            return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        return startDate != null ? startDate.equals(that.startDate) : that.startDate == null;
    }

    @Override
    public int hashCode() {
        int result = attendees != null ? attendees.hashCode() : 0;
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        return result;
    }
}
