package com.example.igalimbikov.myapplication.data.remote.mapper;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.example.igalimbikov.myapplication.data.remote.request.RequestCountryEvent;
import com.example.igalimbikov.myapplication.data.remote.request.ResultRequestBody;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.Partner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Singleton
public class ResultRequestBodyMapper implements Mapper<List<CountryEvent>, ResultRequestBody> {
    @SuppressLint("SimpleDateFormat")
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Inject
    public ResultRequestBodyMapper() {
    }

    @Override
    public ResultRequestBody transform(List<CountryEvent> from) {
        List<RequestCountryEvent> events = new ArrayList<>();
        for (CountryEvent countryEvent : from) {
            events.add(getRequestCountryEvent(countryEvent));
        }
        return new ResultRequestBody(events);
    }

    @NonNull
    private RequestCountryEvent getRequestCountryEvent(CountryEvent countryEvent) {
        String startDate = countryEvent.getStartDate() == null ? null : dateFormat.format(countryEvent.getStartDate());
        int attendeeCount = countryEvent.getAttendees().size();
        List<String> attendeesEmails = getAttendeesEmails(countryEvent);
        String countryName = countryEvent.getCountry().getName();
        return new RequestCountryEvent(attendeeCount, attendeesEmails, countryName, startDate);
    }

    private List<String> getAttendeesEmails(CountryEvent countryEvent) {
        List<String> names = new ArrayList<>();
        for (Partner partner : countryEvent.getAttendees()) {
            names.add(partner.getEmail());
        }
        return names;
    }
}
