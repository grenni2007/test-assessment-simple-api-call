package com.example.igalimbikov.myapplication.data.remote;

import com.example.igalimbikov.myapplication.data.remote.mapper.Mapper;
import com.example.igalimbikov.myapplication.data.remote.model.ApiHubSpotPartners;
import com.example.igalimbikov.myapplication.data.remote.request.ResultRequestBody;
import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.CountryEvent;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;
import com.example.igalimbikov.myapplication.network.MobApiService;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Singleton
public class ExerciseDataSource {
    private static final String API_KEY = "b1a1ba1b1aff41b1e2ad752593ac";

    private final MobApiService service;
    private final Mapper<ApiHubSpotPartners, Map<Country, List<PartnerAvailability>>> exerciseDataMapper;
    private final Mapper<List<CountryEvent>, ResultRequestBody> requestBodyMapper;

    @Inject
    public ExerciseDataSource(MobApiService service,
                              Mapper<ApiHubSpotPartners, Map<Country, List<PartnerAvailability>>> exerciseDataMapper,
                              Mapper<List<CountryEvent>, ResultRequestBody> requestBodyMapper) {
        this.service = service;
        this.exerciseDataMapper = exerciseDataMapper;
        this.requestBodyMapper = requestBodyMapper;
    }

    public Single<Map<Country, List<PartnerAvailability>>> getInput() {
        return service.getPartners(API_KEY)
                .map(new Function<ApiHubSpotPartners, Map<Country, List<PartnerAvailability>>>() {
                    @Override
                    public Map<Country, List<PartnerAvailability>> apply(ApiHubSpotPartners apiHubSpotPartners) throws Exception {
                        return exerciseDataMapper.transform(apiHubSpotPartners);
                    }
                })
                .firstOrError();
    }

    public Completable submitResult(List<CountryEvent> result) {
        return service.submitCountryEventsSchedule(API_KEY, requestBodyMapper.transform(result));
    }
}
