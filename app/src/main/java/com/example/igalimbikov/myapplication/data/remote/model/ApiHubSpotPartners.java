package com.example.igalimbikov.myapplication.data.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public class ApiHubSpotPartners {
    @SerializedName("partners")
    private List<ApiHubSpotPartner> partners;

    public List<ApiHubSpotPartner> getPartners() {
        return partners;
    }
}
