package com.example.igalimbikov.myapplication.data.remote.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public class RequestCountryEvent {
    @SerializedName("attendeeCount")
    private final int attendeeCount;

    @SerializedName("attendees")
    private final List<String> attendees;

    @SerializedName("name")
    private final String name;

    @SerializedName("startDate")
    private final String startDate;

    public RequestCountryEvent(int attendeeCount, List<String> attendees, String name, String startDate) {
        this.attendeeCount = attendeeCount;
        this.attendees = attendees;
        this.name = name;
        this.startDate = startDate;
    }
}
