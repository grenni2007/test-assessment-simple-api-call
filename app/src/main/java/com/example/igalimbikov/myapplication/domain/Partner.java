package com.example.igalimbikov.myapplication.domain;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

public class Partner {
    private final String email;

    public Partner(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Partner partner = (Partner) o;

        return email != null ? email.equals(partner.email) : partner.email == null;
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }
}
