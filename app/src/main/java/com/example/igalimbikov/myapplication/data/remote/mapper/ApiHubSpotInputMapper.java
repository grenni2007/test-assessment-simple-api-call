package com.example.igalimbikov.myapplication.data.remote.mapper;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.example.igalimbikov.myapplication.data.remote.model.ApiHubSpotPartner;
import com.example.igalimbikov.myapplication.data.remote.model.ApiHubSpotPartners;
import com.example.igalimbikov.myapplication.domain.Country;
import com.example.igalimbikov.myapplication.domain.Partner;
import com.example.igalimbikov.myapplication.domain.PartnerAvailability;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by i.galimbikov on 19/11/2017.
 */

@Singleton
public class ApiHubSpotInputMapper implements Mapper<ApiHubSpotPartners, Map<Country, List<PartnerAvailability>>> {
    @SuppressLint("SimpleDateFormat")
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Inject
    public ApiHubSpotInputMapper() {
    }

    @Override
    public Map<Country, List<PartnerAvailability>> transform(ApiHubSpotPartners from) {
        Map<Country, List<PartnerAvailability>> map = new HashMap<>();
        for (ApiHubSpotPartner apiHubSpotPartner : from.getPartners()) {
            Country country = new Country(apiHubSpotPartner.getCountry());
            List<PartnerAvailability> partnerAvailabilities = map.get(country);
            if (partnerAvailabilities == null) {
                partnerAvailabilities = new ArrayList<>();
                map.put(country, partnerAvailabilities);
            }
            partnerAvailabilities.add(parsePartnerAvailability(apiHubSpotPartner));
        }
        return map;
    }

    @NonNull
    private PartnerAvailability parsePartnerAvailability(ApiHubSpotPartner apiHubSpotPartner) {
        String email = apiHubSpotPartner.getEmail();
        List<Date> dates = new ArrayList<>();
        for (String date : apiHubSpotPartner.getDates()) {
            Date parsedDate = parseDate(date);
            if (parsedDate != null) {
                dates.add(parsedDate);
            }
        }
        return new PartnerAvailability(new Partner(email), dates);
    }

    private Date parseDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
